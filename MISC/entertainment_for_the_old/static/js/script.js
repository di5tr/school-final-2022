(function($) {
    $(function()  {
        var puzzleData = [
            {
                clue: "A program for system administration of UNIX systems that allows you to delegate certain privileged resources to users with the maintenance of a work protocol",
                answer: "####",
                position: 0,
                orientation: "across",
                startx: 2,
                starty: 2

            },
            {
                clue: "An algorithm defect that is intentionally built into it by the developer and allows unauthorized access to data or remote control of the operating system and the computer as a whole.",
                answer: "########",
                position: 1,
                orientation: "across",
                startx: 5,
                starty: 5
            },
            {
                clue: "A set of tools used for penetration testing of web applications. It is developed by the company named Portswigger",
                answer: "#########",
                position: 2,
                orientation: "across",
                startx: 1,
                starty: 10
            },
            {
                clue: "A free utility designed for a variety of customized scanning of IP networks with any number of objects, determining the status of objects in the scanned network.",
                answer: "####",
                position: 3,
                orientation: "across",
                startx: 6,
                starty:14
            },
            {
                clue: "Attack consists of an attacker submitting many passwords or passphrases with the hope of eventually guessing correctly",
                answer: "##########",
                position: 4,
                orientation: "across",
                startx: 7,
                starty: 12
            },
            {
                clue: "It was created to provide information about vulnerabilities, help in creating signatures for IDS, creating and testing exploits.",
                answer: "##########",
                position: 5,
                orientation: "across",
                startx: 11,
                starty: 7
            },
            {
                clue: "An amalgamation of two terms denoting the scope of the software: SIM - security information management, and SEM - security event management.",
                answer: "####",
                position: 6,
                orientation: "across",
                startx: 17,
                starty:5
            },
            {
                clue: "A Unix utility that allows you to establish TCP and UDP connections, receive data from there and transmit them.",
                answer: "######",
                position: 7,
                orientation: "across",
                startx: 14,
                starty: 15
            },
            {
                clue: "A type of attack on web systems, which consists in injecting malicious code into a page issued by the web system and interacting with this code with the attacker's web server.",
                answer: "###",
                position: 8,
                orientation: "down",
                startx: 2,
                starty: 1
            },
            {
                clue: "A malicious or erroneously written program that endlessly creates copies of itself, which usually also begin to create copies of themselves, etc.",
                answer: "########",
                position: 9,
                orientation: "down",
                startx: 5,
                starty: 1
            },
            {
                clue: "A bug in a software system where a malicious user can make it execute any machine code they want.",
                answer: "###",
                position: 10,
                orientation: "down",
                startx: 7,
                starty: 4
            },
            {
                clue: "The process of analyzing a string of symbols, either in natural language, computer languages or data structures, conforming to the rules of a formal grammar.",
                answer: "#####",
                position: 11,
                orientation: "down",
                startx: 12,
                starty: 3
            },
            {
                clue: "One of the most common ways to hack sites and programs that work with databases, based on the introduction of an arbitrary SQL code into a query.",
                answer: "####",
                position: 12,
                orientation: "down",
                startx: 17,
                starty: 5
            },
            {
                clue: "Information about other information, or data relating to additional information about the content or object.",
                answer: "########",
                position: 13,
                orientation: "down",
                startx: 20,
                starty: 5
            },
            {
                clue: "A GNU/Linux distribution based on Arch Linux for penetration testing and security assessment that provides tools for analyzing networks and information systems.",
                answer: "#########",
                position: 14,
                orientation: "down",
                startx: 1,
                starty: 10
            },
            {
                clue: "Traffic analyzer program for Ethernet computer networks and some others. Has a graphical user interface. The project was originally called Ethereal",
                answer: "#########",
                position: 15,
                orientation: "down",
                startx: 3,
                starty: 8
            },
            {
                clue: "An archive file that is inherently destructive. When unpacking, it can cause a system crash by filling up all the free space on the media.",
                answer: "#######",
                position: 16,
                orientation: "down",
                startx: 7,
                starty: 9
            },

            {
                clue: "Small piece of code that usually transfers control to the shell",
                answer: "#########",
                position: 17,
                orientation: "down",
                startx: 15,
                starty: 7
            },
            {
                answer: "#########",
                position: 18,
                startx: 15,
                starty: 7
            }
        ]

        $('#puzzle-wrapper').crossword(puzzleData);

    })

    
})(jQuery)

# Entertainment for the old (Scanword)

I used to be and Cyber Security Specialict like you, then I took an exploit in the knee. Could u solve this for me?

# FLAG

MCTF{$canw0rd_r-Pr3tty/3asy}

# Difficulty
Easy

# Type
MISC

# Build and run

    $ docker-compose up --build

or local run:

    $ python3 manage.py run

# Answers
1. sudo
2. backdoor
3. burpsuite
4. nmap
5. bruteforce
6. metasploit
7. siem
8. netcat
9. xss
10. forkbomb
11. rce
12. parse
13. sqli
14. matedata
15. blackarch
16. wireshark
17. zipbomb
18. shellcode

words 13 and 15 start from 7 and 3 accordingly

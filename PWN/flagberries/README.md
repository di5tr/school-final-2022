# Flagberries

— В тасках твоя сила и мудрость.
— Вы любите таски?
— Да.
— А какие сорта предпочитаете?
— Да обычные. Вот, пожалуйста, pwn. Нормальный таск. Пацанский, [УДАЛЕНО]. Решать можно.
— Похвалите ещё таски.
— Ну, как его похвалить, ну, [УДАЛЕНО] таск. [УДАЛЕНО] таск. Как его ещё похвалить, [УДАЛЕНО]? Хе-хе.
— А ещё пара красивых слов?
— [УДАЛЕНО] таск.
— Спасибо.
— Да на здоровье. Вот она, уцуцуга!

[nc localhost 2058]

# Difficulty
Medium

# Type
PWN

# Pre-start

`2058` - NCAT FOR BINARY

Binary: **./app/gotohell**

But, there's compilation script **./run.sh** which contains gcc command to build

# Launch

    # docker-compose build
    # docker-compose up

# Solve

To check if working:
    
    $ python3 exploit.py [REMOTE | LOCAL ]

# Flag
MCTF{0MG-1hats-bAS3D_heap}

# P.S.
nothing

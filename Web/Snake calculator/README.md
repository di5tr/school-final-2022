## Snake Calculator 
If you don't have a calculator to add a couple of numbers together, here is a website to help you!

## Difficulty: medium

## Flag
MCTF{1tS_n0t_sAf3_t0_Us3_0s_c0WWaNdS_D1r3ctLy_1n_M3b}

## PORT
8016

## WriteUp
You can find it in 'WriteUp' folder

## How to start
docker-compose up --build

# Hints
1) There is a hint on a first page.

2) Et cetĕra
# Writeup "Snake calculator"

1) We go to the site and see a form in which you need to enter 2 digits so that the site calculates and displays their amount to us.

![image](./images/Screenshot_1.png)

2) Let's enter some numbers and press the 'Calculate' button

![image](./images/Screenshot_2.png)

3) We will be transferred to the page with the total amount. There is also a 'Return back' button. hmm....................... Let's go back.

![image](./images/Screenshot_3.png)

4) Pay attention to this hint. First number + second number. Maybe when we enter numbers they are transferred to the program? For example: calculate.py firstNumber secondNumber. Let's add a separator to the second number and try to call the whoami command:

![image](./images/Screenshot_4.png)

5) Oh, it printed the user. Excellent!

![image](./images/Screenshot_5.png)

6) Next, you will need to understand that the flag is lying somewhere in the etc folder. So let's list the files in the etc directory.

![image](./images/Screenshot_6.png)

7) So, there is a flag.txt file.

![image](./images/Screenshot_7.png)

8) Let's print it!

![image](./images/Screenshot_8.png)

9) Flag

![image](./images/Screenshot_9.png)
# save this as app.py
from flask import Flask, render_template, request, session
import os, sqlite3
import random
import secrets
import string

app = Flask(__name__)
secret = secrets.token_urlsafe(16)
app.config['SECRET_KEY'] = secret 

def newCookie():
    cookie = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(25))
    session['data'] = cookie
    print(session['data'])

#Home page
@app.route("/")
@app.route("/login")
def hello():
	if not session.get("data"):
		newCookie()
	return render_template("index.html")

#Lins for naive people
@app.route("/forget")
def forget():
	if not session.get("data"):
		newCookie()
	return "IT DOESN'T WORK LIKE THIS >:)"

@app.route("/create")
def create():
	if not session.get("data"):
		newCookie()
	return "YOU SHALL NOT PASS"

#Check user
@app.route("/enter", methods=["POST"])
def check():
	if not session.get("data"):
		newCookie()
	if request.method == "GET":
		return "YA THINK THAT YOU'RE A SMART GUY HERE, HUH?"
	else:
		if request.form['login'] != "" and request.form['password'] != "":
			if  1==1 :
				connect = sqlite3.connect('users.db')
				cursor = connect.cursor()
				login = request.form['login']
				password = request.form['password']
				if ('debug' in request.form and request.form['debug'] == "true") and ('user' in request.form and request.form['user'] == "admin"):
					cursor.execute("SELECT * FROM users WHERE login='{0}' AND password='{1}';".format(login, password))
				else:
					cursor.execute("SELECT * FROM users WHERE login = ? AND password = ?", (str(login), str(password)))
				data = cursor.fetchone()
				connect.commit()
				connect.close()
				if data:
					return "I WANT TO SEE YOUR SAD FACE AFTER SEEING THIS))))))))))"
				else:
					return "HELL IS WAITING FOR YOU"
		else:
			return "ANOTHER SMART PERSON WHO CANNOT TYPE SOMETHING..."


if __name__ == "__main__":
    app.run(debug=False, host=os.environ['IP'], port=os.environ['PORT'])


# save this as app.py
from flask import Flask, render_template, request, make_response
import os, base64
from marshmallow import Schema, fields
import urllib.parse

app = Flask(__name__)

class User(Schema):
	firstName = fields.Str()
	lastName = fields.Str()
	userVK = fields.Str()
	email = fields.Str()
	devil = fields.Str()

#Home page
@app.route("/")
def hello():
    return render_template("index.html")

#Form data accepting and creating serialized cookie encoded base 64 and redirecting to page with sacrifice button
@app.route("/send", methods=["POST", "GET"])
def send():
	string = str(request.form)
	if ('agreed' not in string) or (request.form['agreed'] != 'on'):
		return 'YOU WILL GET NOTHING FOR NOTHING!'
	else:
		if request.form['email'] != '':
			userDict = dict(firstName = request.form['firstName'],lastName = request.form['lastName'],userVK = request.form['userVK'],email = request.form['email'], devil = 'no')
		else:
			userDict = dict(firstName = request.form['firstName'],lastName = request.form['lastName'],userVK = request.form['userVK'], devil = 'no')
		user = User()
		userFin = user.dump(userDict)
		cookie = str(userFin)
		message_bytes = cookie.encode('ascii')
		base64_bytes = base64.b64encode(message_bytes)
		userFinB64 = base64_bytes.decode('ascii')
		resp = make_response(render_template('readcookie.html'))
		resp.set_cookie('sinner', userFinB64)
		return resp

#after clicking sacrifice button here we decode url just in case, decode base 64 and watching if devil is yes -> flag, no - no flag
@app.route("/sacrifice", methods=["POST"])
def sacrifice():
	data = request.cookies.get('sinner')
	data = urllib.parse.unquote(data)
	base64_bytes = data.encode('ascii')
	message_bytes = base64.b64decode(base64_bytes)
	dataEncoded = message_bytes.decode('ascii')
	if "'devil': 'yes'" in dataEncoded:
		return render_template('flag2.html', FLAG = os.environ["FLAG"])
	else:
		return render_template('loozer.html')

#app config
if __name__ == "__main__":
    app.run(debug=False, host=os.environ['IP'], port=os.environ['PORT'])
